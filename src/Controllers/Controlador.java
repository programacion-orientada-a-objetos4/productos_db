package Controllers;

import Models.dbProducto;
import Models.Productos;
import Views.dlgProducto;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener {

    dbProducto db;
    Productos pro;
    dlgProducto vista;
    boolean actualizar;
    boolean habilitar;
    boolean fals;

    public Controlador(dbProducto db, Productos pro, dlgProducto vista) {
        this.db = db;
        this.pro = pro;
        this.vista = vista;

        vista.btnBuscar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnBuscarDes.addActionListener(this);
        vista.btnHabilitar.addActionListener(this);
        vista.btnLimpiarDes.addActionListener(this);
    }

    private void iniciarVista() throws Exception {
        vista.jTProductos.setModel(db.listar());
        vista.jTDes.setModel(db.listarDes());
        vista.setTitle(":: Productos ::");
        vista.setSize(718, 620);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnNuevo) {
            fals = true;
            vista.txtCodigo.setEnabled(fals);
            vista.txtNombre.setEnabled(fals);
            vista.txtPrecio.setEnabled(fals);
            vista.txtCodigoDes.setEnabled(fals);
            vista.txtNombreDes.setEnabled(fals);

            vista.btnBuscar.setEnabled(fals);
            vista.btnDeshabilitar.setEnabled(fals);
            vista.btnGuardar.setEnabled(fals);
            vista.btnCancelar.setEnabled(fals);
            vista.btnCerrar.setEnabled(fals);
            vista.btnLimpiar.setEnabled(fals);
            vista.btnBuscarDes.setEnabled(fals);
            vista.btnHabilitar.setEnabled(fals);

            vista.jDFecha.setEnabled(fals);
            vista.jTProductos.setEnabled(fals);
            vista.jTDes.setEnabled(fals);
        } else if (e.getSource() == vista.btnGuardar) {
            if (valiEmpty() != true) {
                JOptionPane.showMessageDialog(vista, "No deje ningun espacio en blanco");
            } else {
                try {
                    if (actualizar != true) {
                        pro = (Productos) db.buscar(vista.txtCodigo.getText());
                        if (pro.getCodigo().equals("")) {
                            pro.setCodigo(vista.txtCodigo.getText());
                            pro.setNombre(vista.txtNombre.getText());
                            pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                            fecha();
                            db.insertar(pro);
                        } else {
                            JOptionPane.showMessageDialog(vista, "Ese codigo ya existe");
                        }
                    } else {
                        pro.setCodigo(vista.txtCodigo.getText());
                        pro.setNombre(vista.txtNombre.getText());
                        pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                        fecha();
                        db.actualizar(pro);
                        actualizar = false;

                    }
                    vista.jTProductos.setModel(db.listar());
                    limpiar();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error al guardar: " + ex.getMessage());
                }
            }
        } else if (e.getSource() == vista.btnBuscarDes) {
            if (vista.txtCodigoDes.getText().isEmpty()) {
                JOptionPane.showMessageDialog(vista, "Seleccione el codigo que desea buscar");
            } else {
                habilitar = true;
                try {
                    pro = (Productos) db.buscarDes(vista.txtCodigoDes.getText());
                    vista.txtNombreDes.setText(pro.getNombre());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error al buscar los desactivados: " + ex.getMessage());
                }
            }
        } else if (e.getSource() == vista.btnBuscar) {
            if (vista.txtCodigo.getText().isEmpty()) {
                JOptionPane.showMessageDialog(vista, "Seleccione el codigo a buscar");
            } else {
                actualizar = true;
                try {
                    pro = (Productos) db.buscar(vista.txtCodigo.getText());
                    vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                    vista.txtNombre.setText(pro.getNombre());
                    vista.jDFecha.setDate(Date.valueOf(pro.getFecha()));
                    vista.txtNombre.setText(pro.getNombre());

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error al buscar los activados: " + ex.getMessage());
                }
            }
        } else if (e.getSource() == vista.btnDeshabilitar) {
            if (actualizar != true) {
                JOptionPane.showMessageDialog(vista, "Porfavor busque un codigo antes de desabilitar");
            } else {
                try {
                    db.desabilitar(pro);
                    vista.jTProductos.setModel(db.listar());
                    vista.jTDes.setModel(db.listarDes());
                    actualizar = false;
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error al deshabilitar: " + ex.getMessage());;
                }

            }
        } else if (e.getSource() == vista.btnHabilitar) {
            if (habilitar != true) {
                JOptionPane.showMessageDialog(vista, "Porfavor busque un codigo antes de habilitar");
            } else {
                try {
                    db.habilitar(pro);
                    vista.jTDes.setModel(db.listarDes());
                    vista.jTProductos.setModel(db.listar());
                    habilitar = false;
                } catch (Exception ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else if (e.getSource() == vista.btnCancelar) {
            fals = false;
            limpiar();
            vista.txtCodigo.setEnabled(fals);
            vista.txtNombre.setEnabled(fals);
            vista.txtPrecio.setEnabled(fals);
            vista.txtCodigoDes.setEnabled(fals);
            vista.txtNombreDes.setEnabled(fals);

            vista.btnBuscar.setEnabled(fals);
            vista.btnDeshabilitar.setEnabled(fals);
            vista.btnGuardar.setEnabled(fals);
            vista.btnCancelar.setEnabled(fals);
            vista.btnCerrar.setEnabled(true);
            vista.btnLimpiar.setEnabled(fals);
            vista.btnBuscarDes.setEnabled(fals);
            vista.btnHabilitar.setEnabled(fals);

            vista.jDFecha.setEnabled(fals);
            vista.jTProductos.setEnabled(fals);
            vista.jTDes.setEnabled(fals);

        } else if (e.getSource() == vista.btnCerrar) {
            if (JOptionPane.showConfirmDialog(vista, "Seguro que desea cerrar?", "Cerrar", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
            } else {
                vista.setVisible(false);
                vista.dispose();
                System.exit(0);
            }
        } else if (e.getSource() == vista.btnLimpiar || e.getSource() == vista.btnLimpiarDes) {
            limpiar();
            actualizar = false;
        }
    }

    public void fecha() {
        int mes = vista.jDFecha.getCalendar().get(Calendar.MONTH) + 1;
        if (mes < 10) {
            pro.setFecha(String.valueOf(vista.jDFecha.getCalendar().get(Calendar.YEAR)) + ",0" + mes + ","
                    + String.valueOf(vista.jDFecha.getCalendar().get(Calendar.DAY_OF_MONTH)));
        } else {
            pro.setFecha(String.valueOf(vista.jDFecha.getCalendar().get(Calendar.YEAR)) + "," + mes + ","
                    + String.valueOf(vista.jDFecha.getCalendar().get(Calendar.DAY_OF_MONTH)));
        }
    }

    public void limpiar() {
        vista.txtCodigoDes.setText("");
        vista.txtNombreDes.setText("");
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        vista.jDFecha.setDate(null);

    }

    public boolean valiEmpty() {
        return !vista.txtCodigo.getText().isEmpty()
                && !vista.txtNombre.getText().isEmpty()
                && !vista.txtPrecio.getText().isEmpty()
                && vista.jDFecha.getDate() != null;
    }

    public static void main(String[] args) throws Exception {
        dbProducto db = new dbProducto();
        Productos pro = new Productos();
        dlgProducto vista = new dlgProducto();
        Controlador con = new Controlador(db, pro, vista);
        con.iniciarVista();

    }

}
