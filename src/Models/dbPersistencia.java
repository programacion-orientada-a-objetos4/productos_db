/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author andre
 */
public interface dbPersistencia {
    public void insertar(Object objecto) throws Exception;
    public void actualizar(Object objecto) throws Exception;
    public void habilitar(Object objecto) throws Exception;
    public void desabilitar(Object objecto) throws Exception;
    
    public boolean isExiste(int id) throws Exception;
    public DefaultTableModel listar()throws Exception;
    public DefaultTableModel listarDes()throws Exception;
    
    public Object buscarDes(String codigo)throws Exception;
    public Object buscar(String codigo)throws Exception;
    
}
