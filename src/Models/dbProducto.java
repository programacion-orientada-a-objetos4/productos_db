
package Models;
import Views.dlgProducto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class dbProducto extends DbManejador implements dbPersistencia {
    dlgProducto vista;
    @Override
    public void insertar(Object objecto) throws Exception {
        Productos pro = new Productos();

        pro = (Productos) objecto;

        String consulta = "Insert into " + "productos(codigo, nombre, fecha, precio, status)values(?,?,?,?,?)";
        if (this.conectar()) {
            try {

                this.sqlConsulta = conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.setString(2, pro.getNombre());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setFloat(4, pro.getPrecio());
                this.sqlConsulta.setInt(5, pro.getStatus());
                this.sqlConsulta.executeUpdate();
                this.desconectar();

            } catch (SQLException e) {
                JOptionPane.showMessageDialog(vista,"Surgio un error al insertar" +e.getMessage());
            }
        } else {
            JOptionPane.showMessageDialog(vista,"No fue posible conectarse");
        }
    }

    @Override
    public void actualizar(Object objecto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objecto;

        String consulta = "update productos set nombre = ?, precio = ?,"
                + " fecha= ? where codigo = ?";

        if (this.conectar()) {
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                //asignar valores a la consulta

                this.sqlConsulta.setString(1, pro.getNombre());
                this.sqlConsulta.setFloat(2, pro.getPrecio());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setString(4, pro.getCodigo());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
                JOptionPane.showMessageDialog(vista,"Se actualizó correctamente");

            }catch (SQLException e) {
                JOptionPane.showMessageDialog(vista,"Surgio un error al actualizar" +e.getMessage());
            }
        }
    }

    @Override
    public void habilitar(Object objecto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objecto;

        String consulta = "update productos set status = 0 where codigo = ?";

        if (this.conectar()) {
            try {
                JOptionPane.showMessageDialog(vista,"Se habilitó correctamente");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
                
            }catch (SQLException e) {
                JOptionPane.showMessageDialog(vista,"Surgio un error al habilitar" +e.getMessage());
            }
        }
        
    }

    @Override
    public void desabilitar(Object objecto) throws Exception {
       Productos pro = new Productos();
       pro = (Productos) objecto;
        String consulta = "update productos set status = 1 where codigo = ?";

        if (this.conectar()) {
            try {
                JOptionPane.showMessageDialog(vista,"Se deshabilitó correctamente");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                //asignar valores a la consulta
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
                
            }catch (SQLException e) {
                JOptionPane.showMessageDialog(vista,"Surgio un error al deshabilitar" +e.getMessage());
            }
        }
    }

    @Override
    public boolean isExiste(int id) throws Exception {
        return true;
    }

    @Override
    public DefaultTableModel listar() throws Exception {
        Productos pro;
        if(this.conectar()){ 
            String sql = "select * from sistemas.productos where status = 0 order by codigo";
            DefaultTableModel Tabla = new DefaultTableModel();

            JTable VistaTabla = new JTable(Tabla);
            PreparedStatement pst = conexion.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            ResultSetMetaData Datos = rs.getMetaData();

            //Agregar Columnas
            for (int column = 1; column < Datos.getColumnCount(); column++) {
                Tabla.addColumn(Datos.getColumnLabel(column));
            }
            //Agregar Tablas
            while (rs.next()) {
                    Object[] row = new Object[Datos.getColumnCount()];
                    for (int column = 1; column <= Datos.getColumnCount(); column++) {
                        row[column - 1] = rs.getString(column);
                    }
                    Tabla.addRow(row);
                }
            return Tabla;       
        }         
        return null;
    }

    @Override
    public DefaultTableModel listarDes()throws Exception{
        Productos pro;
        if(this.conectar()){ 
            String sql = "select * from sistemas.productos where status = 1 order by codigo";
            DefaultTableModel Tabla = new DefaultTableModel();

            JTable VistaTabla = new JTable(Tabla);
            PreparedStatement pst = conexion.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            ResultSetMetaData Datos = rs.getMetaData();

            //Agregar Columnas
            for (int column = 1; column < Datos.getColumnCount(); column++) {
                Tabla.addColumn(Datos.getColumnLabel(column));
            }
            //Agregar Tablas
            while (rs.next()) {
                    Object[] row = new Object[Datos.getColumnCount()];
                    for (int column = 1; column <= Datos.getColumnCount(); column++) {
                        row[column - 1] = rs.getString(column);
                    }
                    Tabla.addRow(row);
                }
            return Tabla;        
        }
        return null;
         
    }

    @Override
    public Object buscarDes(String codigo) throws Exception {    
       Productos pro = new Productos();
       if(this.conectar()){
            String consultar ="Select * from productos where codigo=? and status=1";
            this.sqlConsulta=this.conexion.prepareStatement(consultar);
            this.sqlConsulta.setString(1, codigo);
            this.Registros=this.sqlConsulta.executeQuery();
            //Sacar los registros
            if(this.Registros.next()){
                pro.setCodigo(this.Registros.getString("codigo"));
                pro.setNombre(this.Registros.getString("nombre"));
                pro.setFecha(this.Registros.getString("fecha"));
                pro.setPrecio(this.Registros.getFloat("precio"));
                pro.setStatus(this.Registros.getInt("status"));
            }
       } 
       this.desconectar();
       return pro;      
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Productos pro = new Productos();
        if(this.conectar()){
            String consultar = "Select * from productos where codigo= ? and status = 0";
            this.sqlConsulta = this.conexion.prepareStatement(consultar);
            //asignar vaores
            this.sqlConsulta.setString(1, codigo);
            //hacer la consulta 
            this.Registros = this.sqlConsulta.executeQuery();
            //sacar los registros
            
            if(this.Registros.next()){
                pro.setCodigo(this.Registros.getString("codigo"));
                pro.setPrecio(this.Registros.getFloat("precio"));
                pro.setFecha(this.Registros.getString("fecha"));
                pro.setStatus(this.Registros.getInt("status"));
                pro.setNombre(this.Registros.getString("nombre"));

            }
        }
        this.desconectar();
        return pro;
    }

   
}
